package com.ceri.uapv1703219.calendrierau;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class PreferenceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        Button valider = findViewById(R.id.valider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radio = findViewById(R.id.choixPromo);
                int id = radio.getCheckedRadioButtonId();
                if(id == -1)
                {
                    //message erreur à géré!
                }
                else
                {
                    RadioButton rad = findViewById(id);
                    String url = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-";
                    if(rad.getText().equals("L3 Informatique"))
                    {
                        url += "L3IN";
                    }
                    else if (rad.getText().equals("L2 Informatique"))
                    {
                        url += "L2IN";
                    }
                    else if(rad.getText().equals("L1 Informatique"))
                    {
                        url += "L1IN";
                    }
                    MainActivity.sharedPreferences = getBaseContext().getSharedPreferences(MainActivity.LIEN_ICAL, MODE_PRIVATE);
                    MainActivity.sharedPreferences.edit()
                            .putString(MainActivity.LIEN_ICAL, url)
                            .apply();

                    DbHelper dbHelper = new DbHelper(PreferenceActivity.this);
                    /*dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 0 ,1);
                    UpdateIcal updateIcal = new UpdateIcal(dbHelper);

                    updateIcal.execute();*/
                    ArrayList<String> groupe =  Parseur.getGroup(dbHelper.getAllEvents());
                    for(int i = 0; i < groupe.size(); i++)
                    {
                        System.out.println(groupe.get(i));
                    }
                    //System.out.println(MainActivity.sharedPreferences.getString(MainActivity.LIEN_ICAL, "Error"));
                }
            }
        });
    }
}
