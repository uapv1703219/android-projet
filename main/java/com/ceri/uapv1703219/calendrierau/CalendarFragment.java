package com.ceri.uapv1703219.calendrierau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import java.util.ArrayList;

public class CalendarFragment extends Fragment {

    CalendarView calendarView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        calendarView = (CalendarView) rootView.findViewById(R.id.calendarView);
        final ArrayList<Event> events = (ArrayList<Event>) this.getArguments().getSerializable("events");


        //Lorsqu'on clique sur une date la transmet et ouvre le fragment day
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                Fragment fragment = new DayFragment();

                bundle.putSerializable("day", new DateO(dayOfMonth, month, year));
                bundle.putSerializable("next", Event.getEventFromDay(events, new DateO(dayOfMonth, month, year)));

                fragment.setArguments(bundle);
                ft.replace(R.id.fragment_container, fragment).commit();
            }
        });

        return rootView;
    }

}
