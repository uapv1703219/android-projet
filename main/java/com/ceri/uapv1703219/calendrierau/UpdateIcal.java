package com.ceri.uapv1703219.calendrierau;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class UpdateIcal extends AsyncTask<Void,Void,Void>{

    public static ArrayList<Event> liste_events = new ArrayList<Event>();
    public static DbHelper dbHelper;

    public UpdateIcal(DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {

            URL url = new URL (MainActivity.sharedPreferences.getString(MainActivity.LIEN_ICAL, "Error"));
            //URL url = new URL("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                //System.out.println(convertStreamToString(in));
                liste_events = Parseur.getVevent(in);
                dbHelper.addAllEvents(liste_events);

                ArrayList<Event> test = new ArrayList<Event>();
                test = dbHelper.getAllEvents();
                System.out.println(test.get(1));
            }
            finally {
                urlConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace(); }
        return null;
    }

    static String convertStreamToString(java.io.InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
