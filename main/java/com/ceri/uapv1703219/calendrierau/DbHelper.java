package com.ceri.uapv1703219.calendrierau;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DbHelper extends SQLiteOpenHelper{

    public static  final String _ID = "_id";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "calendrier.db";
    public static final String COLUMN_MATIERE = "matiere";
    public static final String COLUMN_SALLE = "salle";
    public static final String COLUMN_PROF = "prof";
    public static final String COLUMN_GROUPE = "groupe";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DATE_BEGIN = "date_begin";
    public static final String COLUMN_DATE_END = "date_end";

    public static final String TABLE_NAME = "Calendrier";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_EVENT_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +

                COLUMN_MATIERE + " TEXT NOT NULL, " +
                COLUMN_SALLE + " TEXT, " +
                COLUMN_PROF + " TEXT, " +
                COLUMN_GROUPE + " TEXT NOT NULL, " +
                COLUMN_TYPE + " TEXT, " +
                COLUMN_DATE_BEGIN + " TEXT NOT NULL, " +
                COLUMN_DATE_END + " TEXT NOT NULL );";

        db.execSQL(SQL_CREATE_EVENT_TABLE);
    }

    public boolean addEvent(Event event)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_MATIERE, event.getMatiere());
        values.put(COLUMN_SALLE, event.getSalle());
        values.put(COLUMN_PROF, event.getProf());
        values.put(COLUMN_GROUPE, event.getGroupe());
        values.put(COLUMN_TYPE, event.getType());
        values.put(COLUMN_DATE_BEGIN, event.getDateBegin().toString());
        values.put(COLUMN_DATE_END, event.getDateEnd().toString());

        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        db.close();

        return (rowID != -1);
    }

    public ArrayList<Event> getAllEvents() {
        ArrayList<Event> res = new ArrayList<Event>();
        Cursor cursor = fetchAllEvents();
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            res.add(cursorToEvent(cursor));
            cursor.moveToNext();
        }
        Event.sortEvent(res);
        for (int i = 0; i < res.size(); i++) {
            System.out.println(res.get(i));
        }

        return res;
    }

    public Cursor fetchAllEvents()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null , null);

        //Log.d(TAG, "call fetchAllEvents()");
        if(cursor != null) { cursor.moveToFirst(); }
        return cursor;
    }

    public Event cursorToEvent(Cursor cursor) {
        Event event = new Event(
                cursor.getString(cursor.getColumnIndex(COLUMN_DATE_BEGIN)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DATE_END)),
                cursor.getString(cursor.getColumnIndex(COLUMN_MATIERE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_SALLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_PROF)),
                cursor.getString(cursor.getColumnIndex(COLUMN_GROUPE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TYPE))
        );

        return event;
    }

    public void addAllEvents(ArrayList<Event> events)
    {
        for (int i = 0; i < events.size(); i++) {
            addEvent(events.get(i));
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+ TABLE_NAME);
        onCreate(db);
    }
}
