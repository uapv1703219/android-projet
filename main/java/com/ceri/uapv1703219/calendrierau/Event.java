package com.ceri.uapv1703219.calendrierau;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class Event implements Cloneable, Serializable{

    private static final long serialVersionUID = 1L;
    private DateO dateBegin;
    private DateO dateEnd;
    private String matiere;
    private String salle;
    private String prof;
    private String groupe;
    private String type;

    public Event() {
        this.dateBegin = null;
        this.dateEnd = null;
        this.matiere = "";
        this.salle = "";
        this.prof = "";
        this.groupe = "";
        this.type = "";
    }

    public Event(String date, String heureBegin, String heureEnd, String dateEnd, String matiere, String salle, String prof, String groupe, String type) {
        this.dateBegin = new DateO(date, heureBegin);
        this.dateEnd = new DateO(date, heureEnd);
        this.matiere = matiere;
        this.salle = salle;
        this.prof = prof;
        this.groupe = groupe;
        this.type = type;
    }

    public Event(String dateBegin, String dateEnd, String matiere, String salle, String prof, String groupe, String type) {
        this.dateBegin = new DateO(dateBegin);
        this.dateEnd = new DateO(dateEnd);
        this.matiere = matiere;
        this.salle = salle;
        this.prof = prof;
        this.groupe = groupe;
        this.type = type;
    }

    public static ArrayList<Event> getDateFrom(ArrayList<Event> events, DateO date)
    {
        ArrayList<Event> dateReturn = new ArrayList<Event>();
        for(Event event : events)
        {
            if(event.getDateBegin().compareTo(date) == -1)
            {
                continue;
            }
            dateReturn.add(event);
        }
        return dateReturn;
    }

    //cours dans la journée
    public static ArrayList<Event> getEventFromDay(ArrayList<Event> events, DateO date)
    {
        ArrayList<Event> dateReturn = new ArrayList<Event>();
        for(Event event : events)
        {
            //System.out.println(event);
            if(event.getDateBegin().compareToNoHours(date) == 0)
            {

                dateReturn.add(event);
            }
        }
        return dateReturn;
    }

    public static ArrayList<Event> getAllEvaluations(ArrayList<Event> events)
    {
        Date actuelle = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        String dat = dateFormat.format(actuelle);

        ArrayList<Event> evaluations = new ArrayList<Event>();

        ArrayList<Event> temp = Event.getDateFrom(events, new DateO(dat));

        for(int i = 0; i < temp.size(); i++) {
            //System.out.println(temp.get(i).getType());
            if (temp.get(i).getType().equals("Evaluation")) {
                evaluations.add(temp.get(i));
            }
        }
        Event.sortEvent(evaluations);
        return evaluations;
    }

    public static ArrayList<Event> getTwoEvent(ArrayList<Event> events)
    {
        Date actuelle = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        String dat = dateFormat.format(actuelle);

        ArrayList<Event> twoEvent = new ArrayList<Event>();
        ArrayList<Event> temp = Event.getDateFrom(events, new DateO(dat));
        for(int i = 0; i < 2; i++)
        {
            twoEvent.add(temp.get(i));
        }
        return twoEvent;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getSalle() {
        return salle;
    }

    public void setSalle(String salle) {
        this.salle = salle;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DateO getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String date, String heure) {
        this.dateBegin = new DateO(date, heure);
    }

    public DateO getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String date, String heure) {
        this.dateEnd = new DateO(date, heure);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Event{" +
                "dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", matiere='" + matiere + '\'' +
                ", salle='" + salle + '\'' +
                ", prof='" + prof + '\'' +
                ", groupe='" + groupe + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public static void sortEvent(ArrayList<Event> events)
    {
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event event1, Event event2) {
                // TODO Auto-generated method stub
                return (event1.getDateBegin().compareTo(event2.getDateBegin()));
            }
        });
    }
    //TODO: deux prochains cours à venir (donc par exemple il est 9h, il faut afficher le cours de 10h00-11h30 et le cours de 11h30-13h00)
}