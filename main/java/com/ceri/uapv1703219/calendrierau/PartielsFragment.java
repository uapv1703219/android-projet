package com.ceri.uapv1703219.calendrierau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class PartielsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<Event> events;
    private PartielEventAdapter partielEventAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_partiels, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.partiel_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        events = new ArrayList<Event>();
        events = (ArrayList<Event>) this.getArguments().getSerializable("partiels");

        TextView textView3 = rootView.findViewById(R.id.textView3);
        if(events.size() != 0) { textView3.setText(""); }

        partielEventAdapter = new PartielEventAdapter(events);

        mRecyclerView.setAdapter(partielEventAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }
}
