package com.ceri.uapv1703219.calendrierau;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateO implements Serializable {
	private String annee;
	private String mois;
	private String jour;
	private String heure;
	private String minutes;

	public DateO(String date, String heure)
	{
		this.annee = date.substring(0, 4);
		this.mois = date.substring(4,6);
		this.jour = date.substring(6,8);

		String heureTmp = heure.substring(0,2);
		if(TimeZone.getTimeZone("Europe/Paris").useDaylightTime())
		{
			heureTmp = String.valueOf(Integer.parseInt(heureTmp) + 2);
		}
		else
		{
			heureTmp = String.valueOf(Integer.parseInt(heureTmp) + 1);
		}
		if(heureTmp.length() == 1)
		{
			heureTmp = "0" + heureTmp;
		}

		this.heure = heureTmp;
		this.minutes = heure.substring(2,4);
	}

	public DateO(String date)
	{
		String[] tmp = date.split(" ");

		String[] date2 = tmp[0].split("/");
		this.jour = date2[0];
		this.mois = date2[1];
		this.annee = date2[2];

		String[] heure = tmp[1].split(":");


		this.heure = heure[0];

		this.minutes = heure[1];
	}

	public DateO(int dayOfMonth, int month, int year)
	{
		//System.out.println(dayOfMonth + " / " + month + " / " + year);
		String dayTmp = String.valueOf(dayOfMonth);
		if(dayTmp.length() == 1)
		{
			this.jour = "0" + dayTmp;
		}
		else
		{
			this.jour = dayTmp;
		}
		String monthTmp = String.valueOf(month+1);
		if(monthTmp.length() == 1)
		{
			this.mois = "0" + monthTmp;
		}
		else
		{
			this.mois = monthTmp;
		}
		this.annee = String.valueOf(year);
		this.heure = "00";
		this.minutes = "00";
	}

	public int compareTo(DateO date)
	{
		if(Integer.parseInt(this.annee) < Integer.parseInt(date.getAnnee()))
		{
			return -1;
		}
		else if(Integer.parseInt(this.annee) == Integer.parseInt(date.getAnnee()))
		{
			if(Integer.parseInt(this.mois) < Integer.parseInt(date.getMois()))
			{
				return -1;
			}
			else if(Integer.parseInt(this.mois) == Integer.parseInt(date.getMois()))
			{
				if(Integer.parseInt(this.jour) < Integer.parseInt(date.getJour()))
				{
					return -1;
				}
				else if(Integer.parseInt(this.jour) == Integer.parseInt(date.getJour()))
				{
					if(Integer.parseInt(this.heure) < Integer.parseInt(date.getHeure()))
					{
						return -1;
					}
					else if(Integer.parseInt(this.heure) == Integer.parseInt(date.getHeure()))
					{
						if(Integer.parseInt(this.minutes) < Integer.parseInt(date.minutes))
						{
							return -1;
						}
						else if(Integer.parseInt(this.minutes) == Integer.parseInt(date.minutes))
						{
							return 0;
						}
						else
						{
							return 1;
						}
					}
					else
					{
						return 0;
					}
				}
				else
				{
					return 1;
				}
			}
			else
			{
				return 1;
			}
		}
		else {
			return 1;
		}
	}

	public int compareToNoHours(DateO date)
	{
		if(Integer.parseInt(this.annee) < Integer.parseInt(date.getAnnee()))
		{
			return -1;
		}
		else if(Integer.parseInt(this.annee) == Integer.parseInt(date.getAnnee()))
		{
			if(Integer.parseInt(this.mois) < Integer.parseInt(date.getMois()))
			{
				return -1;
			}
			else if(Integer.parseInt(this.mois) == Integer.parseInt(date.getMois()))
			{
				if(Integer.parseInt(this.jour) < Integer.parseInt(date.getJour()))
				{
					return -1;
				}
				else if(Integer.parseInt(this.jour) == Integer.parseInt(date.getJour()))
				{

					return 0;
				}
				else
				{
					return 1;
				}
			}
			else
			{
				return 1;
			}
		}
		else {
			return 1;
		}
	}

	@Override
	public String toString()
	{
		return this.jour + "/" + this.mois + "/" + this.annee + " " + toStringHour();
	}

	public String toStringDate()
	{
		return this.jour + "/" + this.mois + "/" + this.annee ;
	}

	public String toStringHour()
	{
		return this.heure + ":" + this.minutes;
		/*{
			String heureTmp = String.valueOf(Integer.parseInt(this.heure) + 2);

			if(heureTmp.length() == 1)
			{
				heureTmp = "0" + heureTmp;
			}
			return heureTmp + ":" + this.minutes;
		}
		else
		{
			String heureTmp = String.valueOf(Integer.parseInt(this.heure) + 1);
			if(heureTmp.length() == 1)
			{
				heureTmp = "0" + heureTmp;
			}
			return heureTmp + ":" + this.minutes;
		}*/
	}

	public static String getDate()
	{
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dat = dateFormat.format(actuelle);
		return dat;
	}

	public static DateO getDateO()
	{
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dat = dateFormat.format(actuelle);
		DateO date = new DateO(dat + " " + "00:00");
		return date;
	}

	public String getAnnee() {
		return this.annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getMois() {
		return this.mois;
	}

	public void setMois(String mois) {
		this.mois = mois;
	}

	public String getJour() {
		return this.jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public String getHeure() {
		return this.heure;
	}

	public void setHeure(String heure) {
		this.heure = heure;
	}

	public String getMinutes() {
		return this.minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}
}
