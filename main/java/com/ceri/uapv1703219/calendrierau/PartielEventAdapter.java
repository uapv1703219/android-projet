package com.ceri.uapv1703219.calendrierau;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class PartielEventAdapter extends RecyclerView.Adapter<PartielEventAdapter.PartielViewHolder> {

    ArrayList<Event> events;

    PartielEventAdapter(ArrayList<Event> events) { this.events = events; }


    @NonNull
    @Override
    public PartielEventAdapter.PartielViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.partiel_list_item, viewGroup, false);
        return new PartielEventAdapter.PartielViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PartielEventAdapter.PartielViewHolder partielViewHolder, int i) {
        partielViewHolder.display(events.get(i));
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    class PartielViewHolder extends RecyclerView.ViewHolder {
        private TextView mMatiere;
        private TextView mSalle;
        private TextView mHoraire;
        private TextView mGroup;

        public PartielViewHolder(@NonNull View itemView) {
            super(itemView);

            mMatiere = (TextView) itemView.findViewById(R.id.day_matiere);
            mSalle = (TextView) itemView.findViewById(R.id.day_salle);
            mHoraire = (TextView) itemView.findViewById(R.id.day_horaire);
            mGroup = (TextView) itemView.findViewById(R.id.day_group);
        }

        void display(Event event){
            mMatiere.setText(event.getMatiere());
            mSalle.setText(event.getSalle());
            mHoraire.setText(event.getDateBegin().toString());
            mGroup.setText(event.getGroupe());
        }
    }
}
