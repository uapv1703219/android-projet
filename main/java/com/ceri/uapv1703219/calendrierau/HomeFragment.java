package com.ceri.uapv1703219.calendrierau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    TextView matiere;
    TextView salle;
    TextView professeur;
    TextView type;
    TextView horaire;
    TextView groupe;

    TextView next_matiere;
    TextView next_salle;
    TextView next_type;
    TextView next_horaire;
    TextView next_groupe;

    ArrayList<Event> events = new ArrayList<Event>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        matiere = rootView.findViewById(R.id.matiere);
        salle = rootView.findViewById(R.id.salle);
        professeur = rootView.findViewById(R.id.professeur);
        type = rootView.findViewById(R.id.type);
        horaire = rootView.findViewById(R.id.horaire);
        groupe = rootView.findViewById(R.id.group);

        next_matiere = rootView.findViewById(R.id.next_matiere);
        next_salle = rootView.findViewById(R.id.next_salle);
        next_type = rootView.findViewById(R.id.next_type);
        next_horaire = rootView.findViewById(R.id.next_horaire);
        next_groupe = rootView.findViewById(R.id.next_group);

        events = (ArrayList<Event>) this.getArguments().getSerializable("twonext");
        //System.out.println(events);
        displayTwoEvent(events);
        return rootView;
    }

    public void displayTwoEvent(ArrayList<Event> events)
    {
        Event event1 = events.get(0);
        Event event2 = events.get(1);

        matiere.setText(event1.getMatiere());
        salle.setText(event1.getSalle());
        professeur.setText(event1.getProf());
        type.setText(event1.getType());
        horaire.setText(event1.getDateBegin().toStringHour());
        groupe.setText(event1.getGroupe());

        next_matiere.setText(event2.getMatiere());
        next_salle.setText(event2.getSalle());
        next_type.setText(event2.getType());
        next_horaire.setText(event2.getDateBegin().toStringHour());
        next_groupe.setText(event2.getGroupe());
    }
}
