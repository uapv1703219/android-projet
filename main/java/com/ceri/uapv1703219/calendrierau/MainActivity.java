package com.ceri.uapv1703219.calendrierau;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.ceri.uapv1703219.calendrierau.UpdateIcal.dbHelper;

public class MainActivity extends AppCompatActivity {

    public static SharedPreferences sharedPreferences;
    public static final String LIEN_ICAL = "lien_ical";
    public static final String DATE_LAST_MAJ = "date_last_maj";
    public DbHelper dbHelper = new DbHelper(this);
    public ArrayList<Event> liste_events = new ArrayList<Event>();

    private final String CHANNEL_ID = "time_notifications";
    private final int NOTIFICATION_ID = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //dbHelper.onUpgrade(this.dbHelper.getWritableDatabase(), 0 ,1);
        UpdateIcal updateIcal = new UpdateIcal(dbHelper);
        updateIcal.execute();


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);       //Init le BottomNavigationBar
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);   //Init le selecteur de la BottomNavigationBar


        DateO last_maj_date;
        sharedPreferences = getBaseContext().getSharedPreferences(DATE_LAST_MAJ, MODE_PRIVATE);
        if(MainActivity.sharedPreferences.contains(MainActivity.DATE_LAST_MAJ))
        {
            if ( MainActivity.sharedPreferences.getString(MainActivity.DATE_LAST_MAJ, "Error") != null) {
                last_maj_date = new DateO(MainActivity.sharedPreferences.getString(MainActivity.DATE_LAST_MAJ, "Error"));
            }
            else
            {
                last_maj_date = DateO.getDateO();
            }
        }
        else
        {
            System.out.println(DateO.getDate());
            last_maj_date = DateO.getDateO();
        }
        System.out.println(dbHelper.getAllEvents().size());
        //System.out.println(new DateO(DateO.getDate()).compareToNoHours(last_maj_date));
        if (dbHelper.getAllEvents().size() == 0 || DateO.getDateO().compareToNoHours(last_maj_date) != 0)
        {
            MainActivity.sharedPreferences.edit()
                    .putString(MainActivity.LIEN_ICAL, DateO.getDateO().toString())
                    .apply();
            while (true)
            {
                try {
                    if (updateIcal.get() == null) {
                        System.out.println(updateIcal.getStatus());
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        liste_events = dbHelper.getAllEvents();

        Bundle bundle = new Bundle();
        HomeFragment homeFragment = new HomeFragment();
        bundle.putSerializable("twonext", Event.getTwoEvent(liste_events));
        homeFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();   //Charge le Fragment Home


    }

    //--------------------------SWIPE SECTION ----------------------------------------------

    //TODO: facultatif Effectuer un swipe pour changer de fragment

    //--------------------------BOTTOMNAVIGATIONVIEW SECTION--------------------------------
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =      //Gestion de la BottomNavBar
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;
                    Bundle bundle = new Bundle();

                    switch (menuItem.getItemId()) {
                        case R.id.action_home:
                            selectedFragment = new HomeFragment();
                            setTitle("CalendrierAU");

                            //Passage events
                            bundle.putSerializable("twonext", Event.getTwoEvent(liste_events));
                            selectedFragment.setArguments(bundle);

                            break;
                        case R.id.action_journée:
                            selectedFragment = new DayFragment();
                            setTitle("Journée");

                            //Passage des Events
                            Date actuelle = new Date();
                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm");
                            String dat = dateFormat.format(actuelle);

                            //System.out.println(Event.getEventFromDay(liste_events, new DateO(dat)));
                            bundle.putSerializable("day", new DateO(dat));
                            bundle.putSerializable("next", Event.getEventFromDay(liste_events, new DateO(dat)));
                            selectedFragment.setArguments(bundle);

                            break;
                        case R.id.action_calendrier:
                            selectedFragment = new CalendarFragment();

                            System.out.println(liste_events);
                            bundle.putSerializable("events", liste_events);
                            selectedFragment.setArguments(bundle);
                            setTitle("Calendrier");
                            break;
                        case R.id.action_partiels:
                            selectedFragment = new PartielsFragment();
                            setTitle("Prochains Partiels");

                            displayNotification();

                            //Passage des events
                            ArrayList<Event> evaluations = Event.getAllEvaluations(liste_events);
                            //System.out.println("Pouet " + evaluations.get(0));
                            bundle.putSerializable("partiels", (Serializable) evaluations );
                            selectedFragment.setArguments(bundle);
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();

                    return true;
                }
            };

    //---------------------MENU SECTION----------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Action si Refresh
        if(id == R.id.refresh)
        {
                UpdateIcal updateIcal = new UpdateIcal(dbHelper);
                updateIcal.execute();
        }

        //Action si add_URL
        if (id == R.id.add_url) {
            // custom dialog
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.group_dialog);
            dialog.setTitle("URL Add");

            final EditText editText = (EditText) dialog.findViewById(R.id.editUrl);

            if(!LIEN_ICAL.equals("lien_ical")) {
                editText.setText(LIEN_ICAL.toString());
                Toast.makeText(MainActivity.this, sharedPreferences.getString(LIEN_ICAL, "Error"), Toast.LENGTH_LONG).show();
            }

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the custom dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sharedPreferences = getBaseContext().getSharedPreferences(LIEN_ICAL, MODE_PRIVATE);


                    sharedPreferences.edit()
                            .putString(LIEN_ICAL, editText.getText().toString())
                            .apply();

                    dialog.dismiss();

                    Toast.makeText(MainActivity.this, sharedPreferences.getString(LIEN_ICAL, "Error"), Toast.LENGTH_LONG).show();
                    dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 0, 1);
                    UpdateIcal updateIcal = new UpdateIcal(dbHelper);
                    updateIcal.execute();
                }
            });

            dialog.show();
            return true;
        }
        if (id == R.id.change_group)
        {
            Intent intent = new Intent(this, PreferenceActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    //---------------------------GETTER/SETTER SECTION--------------------------

    public ArrayList<Event> getListe_events() {
        return liste_events;
    }

    public void setListe_events(ArrayList<Event> liste_events) {
        this.liste_events = liste_events;
    }

    //-------------------------------NOTIFICATION SECTION-----------------------
    public void displayNotification()
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_school_black_24dp);
        builder.setContentTitle("Prochain Cours !");
        builder.setContentText("Ton prochains cours a lieu dans 5 minutes !");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat =NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());

    }

    public void update()
    {

    }

}

