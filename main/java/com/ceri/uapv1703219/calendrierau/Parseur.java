package com.ceri.uapv1703219.calendrierau;


/*      BEGIN:VEVENT
    CATEGORIES:HYPERPLANNING
    DTSTAMP:20190424T100637Z
    LAST-MODIFIED:20180517T095250Z
    UID:Cours-74474-3-L3_INFORMATIQUE-Index-Education
    DTSTART:20180911T123000Z
    DTEND:20180911T140000Z
    SUMMARY;LANGUAGE=fr:UE DBWEB 5 PROGRAMMATION WEB - FREDOUILLE - L3 INFORMATIQUE - CM
    LOCATION;LANGUAGE=fr:Amphi Ada
    DESCRIPTION;LANGUAGE=fr:Matière : UE DBWEB 5 PROGRAMMATION WEB\nEnseignant : FREDOUILLE\nPromotion : L3 INFORMATIQUE\nSalle : Amphi Ada\nType : CM\n
    END:VEVENT*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Parseur {

    public static ArrayList<Event> getVevent(InputStream toParse){

        ArrayList<Event> ret = new ArrayList<Event>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(toParse, "UTF-8"));
            String line = reader.readLine();
            Event event_tmp = new Event();

            while(line != null) {
                if (line.contains("BEGIN:VEVENT")) {
                    while (!line.contains("END:VEVENT")) {

                        if (line.contains("DTSTART")) {
                            if(line.contains("VALUE=DATE"))
                            {
                                line = reader.readLine();
                                event_tmp = new Event();
                                break;
                            }

                            String[] info = line.split(":");
                            //System.out.println(Arrays.toString(info));
                            String[] date_heure = info[1].split("T");
                            event_tmp.setDateBegin(date_heure[0], date_heure[1]);
                            //event_tmp.setHeureDebut(date_heure[1].substring(0, date_heure[0].length()-1 ));
                        }

                        else if (line.contains("DTEND")) {
                            String[] info = line.split(":");
                            String[] date_heure = info[1].split("T");
                            event_tmp.setDateEnd(date_heure[0], date_heure[1].substring(0, date_heure[0].length()-1 ));
                        }

                        else if (line.contains("LOCATION")) {
                            String[] info = line.split(":");
                            event_tmp.setSalle(info[1]);
                        }

                        else if(line.contains("UEO") || line.contains("Annulation") || line.contains("Férié") || line.contains("Vacances"))
                        {
                            line = reader.readLine();
                            event_tmp = new Event();
                            continue;
                        }

                        else if (line.contains("SUMMARY")) {
                            String[] info = line.split(":");
                            String[] nom_prof_groupe_type = info[1].split("-");
                            //System.out.println(Arrays.toString(nom_prof_groupe_type));
                            event_tmp.setMatiere(nom_prof_groupe_type[0].substring(0, nom_prof_groupe_type[0].length() - 1));
                            event_tmp.setProf(nom_prof_groupe_type[1].substring(1, nom_prof_groupe_type[1].length() - 1));
                            event_tmp.setGroupe(nom_prof_groupe_type[2].substring(1, nom_prof_groupe_type[2].length() - 1));
                            if (nom_prof_groupe_type.length == 3)
                            {
                                event_tmp.setGroupe(nom_prof_groupe_type[2].substring(1, nom_prof_groupe_type[2].length() - 1));
                                event_tmp.setType("NULL");
                            }
                            else if(nom_prof_groupe_type.length == 2)
                            {
                                event_tmp.setGroupe("NULL");
                                event_tmp.setType("NULL");
                            }
                            else
                            {
                                event_tmp.setGroupe(nom_prof_groupe_type[2].substring(1, nom_prof_groupe_type[2].length() - 1));
                                event_tmp.setType(nom_prof_groupe_type[3].substring(1));
                            }
                        }

                        line = reader.readLine();
                    }

                    //Event add =(Event) event_tmp.clone();
                    if(event_tmp.getDateBegin() != null) {
                        //System.out.println(event_tmp);
                        ret.add((Event) event_tmp.clone());
                        event_tmp = new Event();
                    }

                }
                line = reader.readLine();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static ArrayList<String> getGroup(ArrayList<Event> Vevent)
    {
        ArrayList<String> ret = new ArrayList<String>();
        boolean controle = false;
        for(int i = 0; i < Vevent.size(); i++)
        {
            String groupe = Vevent.get(i).getGroupe();
            String[] listGroup = groupe.split(",");
            for(int j = 0; j < listGroup.length; j++)
            {
                controle = false;
                for(int l = 0; l < ret.size(); l++)
                {
                    if(ret.get(l).contains(listGroup[j]))
                    {
                        controle = true;
                        break;
                    }
                }
                if(!controle)
                {
                    ret.add(listGroup[j]);
                }
            }
        }
        return ret;
    }

}
