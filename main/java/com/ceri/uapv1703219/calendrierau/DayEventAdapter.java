package com.ceri.uapv1703219.calendrierau;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DayEventAdapter extends RecyclerView.Adapter<DayEventAdapter.DayViewHolder> {

    ArrayList<Event> events;

    DayEventAdapter(ArrayList<Event> events)
    {
        this.events = events;
    }

    @NonNull
    @Override
    public DayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.day_list_item, viewGroup, false);
        return new DayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DayViewHolder dayViewHolder, int i) {
        dayViewHolder.display(events.get(i));
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    class DayViewHolder extends RecyclerView.ViewHolder{
        private TextView mMatiere;
        private TextView mSalle;
        private TextView mType;
        private TextView mHoraire;
        private TextView mGroup;
        private TextView mHoraireFin;

        public DayViewHolder(@NonNull View itemView) {
            super(itemView);

            mMatiere = (TextView) itemView.findViewById(R.id.day_matiere);
            mSalle = (TextView) itemView.findViewById(R.id.day_salle);
            mType = (TextView) itemView.findViewById(R.id.day_type);
            mHoraire = (TextView) itemView.findViewById(R.id.day_horaire);
            mGroup = (TextView) itemView.findViewById(R.id.day_group);
            mHoraireFin = (TextView) itemView.findViewById(R.id.heure_end);
        }

        void display(Event event){
            mMatiere.setText(event.getMatiere());
            mSalle.setText(event.getSalle());
            mType.setText(event.getType());
            mHoraire.setText(event.getDateBegin().toString());
            mGroup.setText(event.getGroupe());
            mHoraireFin.setText(event.getDateEnd().toStringHour());
        }
    }
}
