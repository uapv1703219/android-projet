package com.ceri.uapv1703219.calendrierau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DayFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<Event> events = new ArrayList<Event>();
    private DayEventAdapter dayEventAdapter;
    private Date date;
    private int year;
    private int month;
    private int dayOfMonth;
    private TextView dateText;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_day, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.day_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        events = (ArrayList<Event>) this.getArguments().getSerializable("next");
        final DateO date = (DateO) this.getArguments().getSerializable("day");

        TextView textView2 = rootView.findViewById(R.id.textView2);
        if(events.size() != 0) { textView2.setText(""); }

        //Affichage de la bonne date
        dateText = (TextView) rootView.findViewById(R.id.day);
        dateText.setText(date.toStringDate());

        Event.sortEvent(events);
        dayEventAdapter = new DayEventAdapter(events);

        mRecyclerView.setAdapter(dayEventAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

}
